#!/usr/bin/python3

"""
Convert into 'public' directory:
   * *.md
   * *.html
   * *.css
No recursion.
"""
import os
import glob
import markdown
from markdown.extensions.tables import TableExtension

def main():
    converter = markdown.Markdown(extensions=[TableExtension()])
    for filename in glob.glob('*.md'):
        with open(filename, "r", encoding="utf-8") as file_in:
            filename = filename.replace(".md", ".html")
            with open(f'public/{filename}', 'w', encoding="utf-8") as file_out:
                file_out.write(converter.convert(file_in.read()))

    for filename in tuple(glob.glob('*.html')) + tuple(glob.glob('*.css')):
        with open(filename, "r", encoding="utf-8") as file_in:
            with open(f'public/{filename}', 'w', encoding="utf-8") as file_out:
                file_out.write(file_in.read())

if not os.path.exists("public"):
    os.mkdir("public")
main()
